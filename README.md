# Laravel GIT commit checker


## Installation

```bash
composer require wpcmf/git-commit-checker
```

For version <= 5.4:

Add to section `providers` of `config/app.php`:

```php
// config/app.php
'providers' => [
    ...
    Wpcmf\GitCommitChecker\Providers\GitCommitCheckerServiceProvider::class,
];
```

Publish the configuration:

```bash
php artisan vendor:publish --provider="Wpcmf\GitCommitChecker\Providers\GitCommitCheckerServiceProvider" --tag=config
```

### Install GIT hooks
```bash
php artisan git:install-hooks
```

- Create default PSR config (It will create phpcs.xml in your root project).

```bash
php artisan git:create-phpcs
```

- Run test manually (made sure you've added all changed files to git stage)

```bash
php artisan git:pre-commit
```
