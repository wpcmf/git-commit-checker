<?php

namespace Wpcmf\GitCommitChecker\Providers;

use Wpcmf\GitCommitChecker\Commands\InstallHooks;
use Wpcmf\GitCommitChecker\Commands\InstallPhpCs;
use Wpcmf\GitCommitChecker\Commands\PreCommitHook;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallHooks::class,
                PreCommitHook::class,
                InstallPhpCs::class,
            ]);
        }
    }
}
